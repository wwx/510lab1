#include "dsh.h"

typedef struct joblist{
    job_t* jo;
    int job_number;
    bool bg;
    struct joblist* next;
} joblist_t;

joblist_t* jhead = NULL;
int terminal;
extern int dsh_terminal_fd; 
extern bool free_job(job_t *j);

void seize_tty(pid_t callingprocess_pgid); /* Grab control of the terminal for the calling process pgid.  */
void continue_job(job_t *j); /* resume a stopped job */
void spawn_job(job_t *j, bool fg); /* spawn a new job */
bool builtin_cmd(job_t *, int argc, char **argv,bool shouldRun);
char* promptmsg();

void redirect_io(process_t *);
void quit();
void wait_for_fg(job_t* j);
void update_job_numbers();


joblist_t *find_last_job_list (joblist_t *first_job) {
    joblist_t *j = first_job;
    joblist_t* temp;
    if(!j) return NULL;
    while(j->next != NULL) {
        if (job_is_stopped(j -> jo) && !job_is_completed(j -> jo)) {
            temp = j;
        }
        j = j->next;
    }


    if (job_is_stopped(j -> jo)  && !job_is_completed(j -> jo)) {
        temp = j;
    }

    return temp;
}


void sigint_handler(int sig){

    switch (sig){
        case SIGINT:
            quit();
        default:
            return;
    }
        
}

/* Sets the process group id for a given job and process */
int set_child_pgid(job_t *j, process_t *p)
{
    if (j->pgid < 0) /* first child: use its pid for job pgid */
        j->pgid = p->pid;
    
    return(setpgid(p->pid,j->pgid));
}

/* Creates the context for a new child by setting the pid, pgid and tcsetpgrp */
void new_child(job_t *j, process_t *p, bool fg)
{
    /* establish a new process group, and put the child in
     * foreground if requested
     */

    /* Put the process into the process group and give the process
     * group the terminal, if appropriate.  This has to be done both by
     * the dsh and in the individual child processes because of
     * potential race conditions.  
     * */

    p->pid = getpid();

    /* also establish child process group in child to avoid race (if parent has not done it yet). */
    set_child_pgid(j, p);

    if(fg) // if fg is set
        seize_tty(j->pgid); // assign the terminal

    /* Set the handling for job control signals back to the default. */
    signal(SIGTTOU, SIG_DFL);
}

void update_status(process_t* p, bool stopped, bool terminated){
    p->stopped = stopped;
    p->completed = terminated;
}

void sigchld_handler(int sig){
    joblist_t* curr = jhead;
    //for all jobs
    while(curr){
        bool interrupted = false;
        //if the job is in the background and hasn't finished yet
        if (curr->bg){
            if(job_is_completed(curr->jo)||job_is_stopped(curr->jo)){
                curr = curr->next;
                continue;
            }
            //update the status of each process
            process_t* p;
            
            
            for (p = curr->jo->first_process;p;p = p->next){
                int status;
                pid_t p1;
                pid_t p2;
                switch(p1 = waitpid(p->pid,&status,WNOHANG)){
                    case 0: //if there is no status, it's because it's either sleeping or still running
                        
                        p2 = waitpid(p->pid,&status,WNOHANG||WUNTRACED);
                        if (!p2){//if there is no status now, it's definitely running
                            update_status(p,false,false);
                        }else{
                            update_status(p,true,false);//sleeping
                        }
                        break;
                    default: //if a status is available here, it's because process has finished running
                        update_status(p,false,true);
                        if (WIFEXITED(status)!=0){
                        }else if (WIFSIGNALED(status)!=0){
                            interrupted = true;
                        }
                }
            }
            if (job_is_completed(curr->jo)){
                if(interrupted){
                    fprintf(stdout," %d(Killed by Signal): %s\n%s",(int)curr->jo->pgid,curr->jo->commandinfo,promptmsg());
                    fflush(stdout);
                }else{
                    fprintf(stdout," %d(Exited Normally): %s\n%s",(int)curr->jo->pgid,curr->jo->commandinfo,promptmsg());

                    fflush(stdout);
                }
                return;
            }else if (job_is_stopped(curr->jo)){
                fprintf(stdout," %d(Stopped): %s\n%s",(int)curr->jo->pgid,curr->jo->commandinfo,promptmsg());

                fflush(stdout);
                return;
            }

        }
        
        curr = curr->next;
    }
}

/* Spawning a process with job control. fg is true if the 
 * newly-created process is to be placed in the foreground. 
 * (This implicitly puts the calling process in the background, 
 * so watch out for tty I/O after doing this.) pgid is -1 to 
 * create a new job, in which case the returned pid is also the 
 * pgid of the new job.  Else pgid specifies an existing job's 
 * pgid: this feature is used to start the second or 
 * subsequent processes in a pipeline.
 * */

void spawn_job(job_t *j, bool fg) 
{
    pid_t pid;
    process_t *p;
    bool builtin = false;
    int prev_pipe[2];
    int next_pipe[2];

    for(p = j->first_process; p; p = p->next) {
        if(builtin_cmd(j, p->argc, p->argv,true)) {
            builtin = true;
            continue;
        }

        //need to pipe to next process
        if(p->next) {
            pipe(next_pipe);
        }

        /* YOUR CODE HERE? */
        /* Builtin commands are already taken care earlier */
        //signal (SIGCHLD,sigchild_handler);
        switch (pid = fork()) {

            case -1: /* fork failure */
                perror("fork");
                exit(EXIT_FAILURE);

            case 0: /* child process  */
                p->pid = getpid();     
                new_child(j, p, fg);
                //handle piping
                if(p->next) {
                    //next_pipe[0] is read, next_pipe[1] is write
                    close(next_pipe[0]);
                    dup2(next_pipe[1], STDOUT_FILENO);
                    close(next_pipe[1]);
                }
                if(p != j->first_process) {
                    close(prev_pipe[1]);
                    dup2(prev_pipe[0], STDIN_FILENO);
                    close(prev_pipe[0]);
                }


                //handle io redirection
                redirect_io(p);

                //execute command
                execvp(*(p->argv), p->argv);
                perror("New child should have done an exec");
                exit(EXIT_FAILURE);  /* NOT REACHED */
                break;    /* NOT REACHED */

            default: /* parent */
                /* establish child process group */
                p->pid = pid;
                set_child_pgid(j, p);
                if(pid == j->pgid) {
                    fprintf(stderr, "%d(Launched): %s\n", pid, j->commandinfo);
                }
                if(p != j->first_process) {
                    close(prev_pipe[0]);
                    close(prev_pipe[1]);
                }
                prev_pipe[0] = next_pipe[0];
                prev_pipe[1] = next_pipe[1];

        }
        seize_tty(getpid()); // assign the terminal back to dsh
    }
    /*suspend execution until all processes are either sleeping or terminated
    since fg, then update their statuses accordingly once we get control
    back*/
    if (fg&&!builtin){
        wait_for_fg(j);
    }

}
/* waits for the job specified by j to either terminate or go
to sleep, then frees all zombie children*/
void wait_for_fg(job_t* j){
    process_t* p;
    for (p = j->first_process;p;p=p->next){
            pid_t n;
            int status;
            switch (n=waitpid(p->pid,&status,WUNTRACED)){
                case -1:
                    fprintf(stdout,"ERROR\n");
                    fflush(stdout);
                    break;
                default: 
                    switch (n = waitpid(p->pid,NULL,WNOHANG)){
                        case 0:
                            update_status(p,true,false);
                            break;
                        case -1:
                            update_status(p,false,true);
                            if (WIFEXITED(status)!=0){
                                fprintf(stdout,"%d(Exited Normally): %s\n",(int)j->pgid,j->commandinfo);
                            }else{
                                if (WIFSIGNALED(status)!=0){
                                    fprintf(stdout,"\n%d(Killed by Signal): %s\n",(int)j->pgid,j->commandinfo);
                                }
                            }
                            break;
                        default:
                            fprintf(stdout,"ERROR\n");
                            fflush(stdout);
                    }

            }
        }
        seize_tty(getpid());   
}

/* Sends SIGCONT signal to wake up the blocked job */
void continue_job(job_t *j) 
{
    if(kill(-j->pgid, SIGCONT) < 0)
        perror("kill(SIGCONT)");
}

void quit(){
    joblist_t* curr = jhead;
    while(curr){
        joblist_t* temp = curr;
        curr = curr->next;
        free_job(temp->jo);
        free(temp);
    }
    exit(0);
}
//this code is awesome

/* 
 * builtin_cmd - If the user has typed a built-in command then execute
 * it immediately.
 */ 
bool builtin_cmd(job_t *last_job, int argc, char **argv, bool shouldRun) 
{

    process_t* process = last_job ->first_process;
    if (!strcmp(argv[0], "quit")) {
        if (!shouldRun){
            return true;
        }
        quit();
    }
    else if (!strcmp("jobs", argv[0])) {
        if (!shouldRun){
            return true;
        }
        int counter = 0;
        joblist_t* curr = jhead;
        joblist_t* prev = NULL;
        while(curr!=NULL){
            if (job_is_completed(curr->jo)){
                if (!counter){
                    fprintf(stdout,"\n");
                }
                counter++;
                fprintf(stdout,"%d(Completed): %s\n",(int)curr->jo->pgid,curr->jo->commandinfo);
                fflush(stdout);
                if (curr == jhead){
                    jhead = jhead->next;
                    free_job(curr->jo);
                    free(curr);
                    curr = jhead;
                    if (!curr){
                        break;
                    }
                    continue;
                }else{
                    prev->next = curr->next;
                    free_job(curr->jo);
                    free(curr);
                    curr = prev->next;
                    if(!curr){
                        break;
                    }
                    continue;
                }
            }else if (job_is_stopped(curr->jo)){
                if (!counter){
                    fprintf(stdout,"\n");
                }
                counter++;
                fprintf(stdout,"%d(Stopped): %s\n",(int)curr->jo->pgid,curr->jo->commandinfo);
                fflush(stdout);
            }else{
                if (!counter){
                    fprintf(stdout,"\n");
                }
                counter++;
                int status;
                job_t* j = curr->jo;
                process_t* p;
                for (p = j->first_process;p;p = p->next){
                    waitpid(p->pid,&status,WNOHANG);
                    int stoppedint = WIFSTOPPED(status);
                    int endedint = WIFEXITED(status);
                    if (endedint!=0){
                        p->completed = true;
                    }else if (stoppedint!=0){
                        p->stopped = true;
                    }
                }
                if (job_is_stopped(j)||job_is_completed(j)){
                    continue;
                }else if(curr->jo->pgid==(pid_t)-1){
                    fprintf(stdout,"(Unassigned): %s\n",curr->jo->commandinfo);
                }else{
                    fprintf(stdout,"%d(Running): %s\n",(int)curr->jo->pgid,curr->jo->commandinfo);
                    fflush(stdout);
                }

            }
            prev = curr;
            curr = curr->next;
        }
        if (counter>0){
            fprintf(stdout,"\n");
        }
        return true;
    }
    else if (!strcmp("cd", argv[0])) {
        if (!shouldRun){
            return true;
        }
        /* Your code here */
        if(argc < 2) {
            chdir(getenv("HOME"));
        }
        else {
            chdir(argv[1]);
        }
        return true;
    }
    else if (!strcmp("bg", argv[0])) {
        if (!shouldRun){
            return true;
        }
        joblist_t* job_to_alive;
        if (argc == 1) {
            job_to_alive = find_last_job_list(jhead);
        } else {
            int job_number = atoi(argv[1]);
            job_to_alive = NULL;
            joblist_t *j;
            for (j = jhead; j; j = j->next) {
                if(j->jo->pgid == job_number) {
                    job_to_alive = j;
                }
            }
        }

        if (job_to_alive == NULL) 
            return true;

        job_to_alive->bg = true;
        process_t* p;
        for (p = job_to_alive->jo->first_process;p;p=p->next){
            update_status(p,false,false);
        }

        continue_job(job_to_alive->jo);

        return true;
    }
    else if (!strcmp("fg", argv[0])) {
        if (!shouldRun){
            return true;
        }
        pid_t d_pgid = tcgetpgrp(dsh_terminal_fd);
        joblist_t* job_to_alive;
        if (argc == 1) {
            job_to_alive = find_last_job_list(jhead);
        } else {
            int job_number = atoi(argv[1]);
            job_to_alive = NULL;
            joblist_t *j;
            for (j = jhead; j; j = j->next) {
                if(j->jo->pgid == job_number) {
                    job_to_alive = j;
                }
            }
        }

        if (job_to_alive == NULL) 
            return true;
        process_t* p;
        for (p = job_to_alive->jo->first_process;p;p=p->next){
            update_status(p,false,false);
        }
        job_to_alive->bg = false;
        tcsetpgrp(dsh_terminal_fd, job_to_alive->jo->pgid);
        continue_job(job_to_alive->jo);
        wait_for_fg(job_to_alive->jo);
        tcsetpgrp(dsh_terminal_fd, d_pgid);

        process->completed = true;

        return true;
    }
    return false;       /* not a builtin command */
}

/* Build prompt messaage */
char* promptmsg() 
{
    const char *shell = "dsh -";
    size_t len = strlen(shell);
    char *msg = (char*) malloc(sizeof(int) * 10 + len);
    sprintf(msg, "%s%d$ ", shell, getpid());
    return msg;
}

void redirect_io(process_t *proc) {
    char *in = proc->ifile;
    char *out = proc->ofile;
    int in_fd, out_fd;
    in_fd = out_fd = -1;
    //if input redirection is used, "in" will not be null
    if(in) {
        in_fd = open(in, O_RDONLY);
        //STDIN_FILENO is fd for stdin
        //in_fd is mapped to stdin, so command reads from stdin
        dup2(in_fd, STDIN_FILENO);
        close(in_fd);
    }
    //if output redirection is used, "out" will not be null
    if(out) {
        //TODO not sure how to create file with default permissions
        out_fd = open(out, O_RDWR | O_CREAT, 0775);
        //STDOUT_FILENO is fd for stdout
        //out_fd is mapped to stdout, so command writes to file
        dup2(out_fd, STDOUT_FILENO);
        close(out_fd);
    }
}

int main() 
{
    init_dsh();
    DEBUG("Successfully initialized\n");
    signal(SIGINT,(void *) sigint_handler);
    signal(SIGCHLD,(void *) sigchld_handler);
    while(1) {
        job_t *j = NULL;
        char *msg = promptmsg();
        if(!(j = readcmdline(msg))) {
            if (feof(stdin)) { /* End of file (ctrl-d) */
                fflush(stdout);
                printf("\n");
                //clean up all memory allocations etc.
                quit();
                exit(EXIT_SUCCESS);
            }
            continue; /* NOOP; user entered return or spaces with return */
        }

        free(msg);

        job_t* t = j;
        while(t){
            if (builtin_cmd(t,t->first_process->argc,t->first_process->argv,false)){
                t = t->next;
                continue;
            }
            //add to joblist
            joblist_t* newjob = (joblist_t*) malloc(sizeof(joblist_t));
            newjob->jo = t;
            newjob->job_number = -1;
            newjob->next = NULL;
            if (t->bg){
                newjob->bg = true;
            }else{
                newjob->bg = false;
            }
            if(!jhead){
                jhead = newjob;
            }else{
                joblist_t* curr = jhead;
                while(curr->next){
                    curr = curr->next;
                }
            curr->next = newjob;
            }
            //also check bg condition 
            t = t->next;   
        }
        update_job_numbers();
        while(j){
            spawn_job(j,!j->bg);
            j = j->next;
        }
        
        /* Only for debugging purposes to show parser output; turn off in the
         * final code */
        /*if(PRINT_INFO) print_job(j);*/
    }
}

void update_job_numbers(){
    joblist_t* curr = jhead;
    int i=0;
    while(curr){
        if (curr->job_number==-1){
            bool free = false;
            while(!free){
                i++;
                free = true;
                joblist_t* checker = jhead;
                while(checker){
                    if (checker->job_number==i){
                        free= false;
                        break;
                    }
                    checker = checker->next;
                }
            }
            curr->job_number = i;
        }
        
        curr = curr->next;
    }
    return;
}
